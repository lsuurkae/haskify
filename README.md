# README #

To open and run this application you need Visual Studio 2015 and you need [GHC](https://www.haskell.org/ghc/) to be installed on your computer. 

### What is Haskify? ###

* Haskify is a great application where people who are not good at Haskell can improve their skills. 

### Contribution guidelines ###

* Writing tests
* Code review
* Adding more exercises to application

### Who do I talk to? ###

* Repo owner or admin