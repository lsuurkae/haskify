﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Haskify.Models;
using Haskify.Services;
using Moq;

namespace Haskify.Tests
{
    [TestFixture]
    public class ExerciseServiceTests
    {
        [Test]
        public void GetExercise_Does_Not_Crash_When_File_Not_Found()
        {
            var sut = new ExerciseService(Mock.Of<IHaskellService>());

            // Act
            var result = sut.GetExercise("Math","1");

            // Assert
            Assert.That(result, Is.Null);
        }
        
    }
}
