--Category: %Math
--Title: %Negate Boolean
{-Description: 
Make a function 'negateBoolean' that returns the negation of the boolean value.
-}
{-Hint:
Use "not" operator!
-}

import Prelude hiding (IO)
import Test.HUnit

test1 = TestCase (assertEqual "True" (False) (negateBoolean True))
test2 = TestCase (assertEqual "False" (True) (negateBoolean False))
tests = TestList [TestLabel "test1" test1, TestLabel "test2" test2]

main = do runTestTT tests

{-Solution:
negateBoolean a = not a 
-}

{-Code: 
negateBoolean a =
-}