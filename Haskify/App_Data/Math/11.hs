--Category: %Math
--Title: %Maximum
{-Description: 
Make a function 'maximumOf' that takes the maximum of two numbers.
-}
{-Hint:
Use "max" operator.
-}

import Prelude hiding (IO)
import Test.HUnit

test1 = TestCase (assertEqual "maximum of 3 and 4" (4) (maximumOf 3 4))
test2 = TestCase (assertEqual "maximum of 5 and 5" (5) (maximumOf 5 5))
test3 = TestCase (assertEqual "maximum of -6 and -8" (-6) (maximumOf (-6) (-8)))
tests = TestList [TestLabel "test1" test1, TestLabel "test2" test2, TestLabel "test3" test3]

main = do runTestTT tests

{-Solution: 
maximumOf a b = max a b
-}

{-Code: 
maximumOf a b =
-}