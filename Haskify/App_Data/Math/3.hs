--Category: %Math
--Title: %Division
{-Description: 
Make a function 'divideByTwo' that divides numbers by 2.
-}
{-Hint:
Use "/" operator!
-}

import Prelude hiding (IO)
import Test.HUnit

test1 = TestCase (assertEqual "12 / 2" (6) (divideByTwo 12))
test2 = TestCase (assertEqual "5 / 2" (2.5) (divideByTwo 5))
test3 = TestCase (assertEqual "-6 / 2" (-3) (divideByTwo (-6)))
tests = TestList [TestLabel "test1" test1, TestLabel "test2" test2, TestLabel "test3" test3]

main = do runTestTT tests

{-Solution:
divideByTwo a = a / 2
-}

{-Code: 
divideByTwo a =
-}