--Category: %Math
--Title: %Sum
{-Description: 

Make a function 'sumOf' that calculates the sum of two numbers. 
-}
{-Hint:
Use "+" operator!
-}

import Prelude hiding (IO)
import Test.HUnit
import Test.QuickCheck

test1 = TestCase (assertEqual "3 + 10" (13) (sumOf 3 10))
test2 = TestCase (assertEqual "5 + 4" (9) (sumOf 5 4))
test3 = TestCase (assertEqual "-5 + 4" (-1) (sumOf (-5) 4))
tests = TestList [TestLabel "test1" test1, TestLabel "test2" test2, TestLabel "test3" test3]

prop_commutativeAdd :: Integer -> Integer -> Bool
prop_commutativeAdd n m = (sumOf n m) == (sumOf m n)

prop_correctAdd :: Integer -> Integer -> Bool
prop_correctAdd n m = (sumOf n m) == (n + m)

main = do runTestTT tests

{-Solution:
sumOf a b = a + b
-}
sumOf :: (Num a) => a -> a -> a
{-Code: 
sumOf a b =
-}