--Category: %Math
--Title: %Boolean And
{-Description: 
Make a function 'booleanAnd' that returns the logic AND.
-}
{-Hint:
Use "&&" operator!
-}

import Prelude hiding (IO)
import Test.HUnit

test1 = TestCase (assertEqual "True True" (True) (booleanAnd True True))
test2 = TestCase (assertEqual "False True" (False) (booleanAnd False True))
test3 = TestCase (assertEqual "True False" (False) (booleanAnd True False))
test4 = TestCase (assertEqual "False False" (False) (booleanAnd False False))
tests = TestList [TestLabel "test1" test1, TestLabel "test2" test2, TestLabel "test3" test3, TestLabel "test4" test4]

main = do runTestTT tests

{-Solution:
booleanAnd a b = a && b
-}

{-Code: 
booleanAnd a b=
-}