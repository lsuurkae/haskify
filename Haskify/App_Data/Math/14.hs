--Category: %Math
--Title: %Tee kõik katki
{-Description: 
Make a function that teeb kõik katki
-}
{-Hint:
Use "+" operator
-}

import Prelude hiding (IO)
import Test.HUnit

test1 = TestCase (assertEqual "3 + 10" (13) (addTen 3))
tests = TestList [TestLabel "test1" test1]

main = do runTestTT tests

{-Solution: 
addTen a = a + 10
-}

{-Code: 
addTen :: (Integral a) => a -> a
addTen a = factorial 10000

factorial :: (Integral a) => a -> a  
factorial 0 = 1  
factorial n = n * factorial (n - 1)
-}