--Category: %Math
--Title: %Multiplication
{-Description: 
Make a function 'multiply' that multiplies two numbers
-}
{-Hint:
Use "*" operator!
-}

import Prelude hiding (IO)
import Test.HUnit

test1 = TestCase (assertEqual "12 * 2" (24) (multiply 12 2))
test2 = TestCase (assertEqual "4 * 2.5" (10) (multiply 4 2.5))
test3 = TestCase (assertEqual "-6 * 2" (-12) (multiply (-6) 2))
tests = TestList [TestLabel "test1" test1, TestLabel "test2" test2, TestLabel "test3" test3]

main = do runTestTT tests

{-Solution:
multiply a b = a * b
-}

{-Code: 
multiply a b =
-}