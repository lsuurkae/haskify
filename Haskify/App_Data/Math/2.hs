--Category: %Math
--Title: %Subtraction
{-Description: 
Make a function 'subtractNums' that subtracts one number from the other.
-}
{-Hint:
Use "-" operator!
-}

import Prelude hiding (IO)
import Test.HUnit

test1 = TestCase (assertEqual "13 - 10" (3) (subtractNums 13 10))
test2 = TestCase (assertEqual "5 - 4" (1) (subtractNums 5 4))
test3 = TestCase (assertEqual "-5 - 4" (-9) (subtractNums (-5) 4))
tests = TestList [TestLabel "test1" test1, TestLabel "test2" test2, TestLabel "test3" test3]

main = do runTestTT tests

{-Solution:
subtractNums a b = a - b
-}

{-Code: 
subtractNums a b =
-}