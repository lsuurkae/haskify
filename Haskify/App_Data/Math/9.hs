--Category: %Math
--Title: %Add one
{-Description: 
Make a function 'addOne' that adds 1 to a number
-}
{-Hint:
Use "succ" or "+" operator.
-}

import Prelude hiding (IO)
import Test.HUnit

test1 = TestCase (assertEqual "3 + 1" (4) (addOne 3))
test2 = TestCase (assertEqual "0 + 1" (1) (addOne 0))
test3 = TestCase (assertEqual "-4 + 1" (-3) (addOne (-4)))
tests = TestList [TestLabel "test1" test1, TestLabel "test2" test2, TestLabel "test3" test3]

main = do runTestTT tests

{-Solution: 
addOne a = succ a
-}

{-Code: 
addOne a =
-}