--Category: %Math
--Title: %Boolean Or
{-Description: 
Make a function 'booleanOr' that returns the logic OR.
-}
{-Hint:
Use "||" operator!
-}

import Prelude hiding (IO)
import Test.HUnit

test1 = TestCase (assertEqual "True True" (True) (booleanOr True True))
test2 = TestCase (assertEqual "False True" (True) (booleanOr False True))
test3 = TestCase (assertEqual "True False" (True) (booleanOr True False))
test4 = TestCase (assertEqual "False False" (False) (booleanOr False False))
tests = TestList [TestLabel "test1" test1, TestLabel "test2" test2, TestLabel "test3" test3, TestLabel "test4" test4]

main = do runTestTT tests

{-Solution:
booleanOr a b = a || b
-}

{-Code: 
booleanOr a b=
-}