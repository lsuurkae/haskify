--Category: %Math
--Title: %Square Root
{-Description: 
Make a function 'squareRootOf' that calculates a square root of a number!
-}
{-Hint:
Use "sqrt" operator!
-}

import Prelude hiding (IO)
import Test.HUnit

test1 = TestCase (assertEqual "sqrt 16" (4) (squareRootOf 16))
test2 = TestCase (assertEqual "sqrt 1" (1) (squareRootOf 1))
test3 = TestCase (assertEqual "sqrt 25" (5) (squareRootOf 25))
tests = TestList [TestLabel "test1" test1, TestLabel "test2" test2, TestLabel "test3" test3]

main = do runTestTT tests

{-Solution:
squareRootOf a = sqrt a
-}

{-Code: 
squareRootOf a =
-}