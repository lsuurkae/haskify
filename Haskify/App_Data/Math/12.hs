--Category: %Math
--Title: %Add ten
{-Description: 
Make a function that adds 10 to a number
-}
{-Hint:
Use "+" operator
-}

import Prelude hiding (IO)
import Test.HUnit

test1 = TestCase (assertEqual "3 + 10" (13) (addTen 3))
test1 = TestCase (assertEqual "0 + 10" (10) (addTen 0))
test1 = TestCase (assertEqual "-3 + 10" (7) (addTen (-3)))
tests = TestList [TestLabel "test1" test1, TestLabel "test2" test2, TestLabel "test3" test3]

main = do runTestTT tests

{-Solution: 
addTen a = a + 10
-}

{-Code: 
addTen a = a
-}