--Category: %Math
--Title: %Hypotenuse
{-Description: 
Make a function that calculates hypotenuse
-}
{-Hint:
sqrt
-}

import Prelude hiding (IO)
import Test.HUnit

test1 = TestCase (assertEqual "a=3, b=4, c=?" (5) (hypotenuse 3 4))
tests = TestList [TestLabel "test1" test1]

main = do runTestTT tests

{-Solution: 
hypotenuse x y = sqrt (x*x + y*y)
-}

{-Code: 
hypotenuse :: Double -> Double -> Double
-}
