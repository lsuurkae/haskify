--Category: %Math
--Title: %Triple a number
{-Description: 
Make a function 'squareArea' that calculates the area of a square. 
The formula for calculating square are is S = a * a.
-}
{-Hint:
Use "*" operator. 
-}

import Prelude hiding (IO)
import Test.HUnit

test1 = TestCase (assertEqual "a = 3" (9) (squareArea 3))
test2 = TestCase (assertEqual "a = 2.5" (6.25) (squareArea 2.5))
test3 = TestCase (assertEqual "0" (0) (squareArea 0))
tests = TestList [TestLabel "test1" test1, TestLabel "test2" test2, TestLabel "test3" test3]

main = do runTestTT tests

{-Solution: 
squareArea a = a * a
-}

{-Code: 
squareArea
-}