--Category: %Math
--Title: %Minimum
{-Description: 
Make a function 'minimumOf' that takes the minimum of two numbers.
-}
{-Hint:
Use "min" operator.
-}

import Prelude hiding (IO)
import Test.HUnit

test1 = TestCase (assertEqual "minimum of 3 and 4" (3) (minimumOf 3 4))
test2 = TestCase (assertEqual "minimum of 5 and 5" (5) (minimumOf 5 5))
test3 = TestCase (assertEqual "minimum of -6 and -8" (-8) (minimumOf (-6) (-8)))
tests = TestList [TestLabel "test1" test1, TestLabel "test2" test2, TestLabel "test3" test3]

main = do runTestTT tests

{-Solution: 
minimumOf a b = min a b
-}

{-Code: 
minimumOf a b =
-}