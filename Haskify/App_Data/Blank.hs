﻿--Category: %CATEGORY
--Title: %TITLE
{-Description: 
EXERCISE DESCRIPTION
-}
{-Hint:
HINT
-}

import Test.HUnit
--or import Test.QuickCheck

test1 = TestCase (assertEqual "TEST DESCRIPTION" (EXPECTED) (ACTUAL))
tests = TestList [TestLabel "test1" test1]

main = do runTestTT tests
-- or main = do quickCheck propertyToTest

{-Solution: 
SOLUTION
-}

{-Code: 
INITIAL CODE
-}