--Category: %Lists
--Title: %Get first
{-Description: 
Make a function 'getFirst' that returns the first element from the list.
-}
{-Hint:
Use 'head' function or
as we know list x == (x:xs), 
where x is the head and xs the body of the list.
-}

import Prelude hiding (IO)
import Test.HUnit

test1 = TestCase (assertEqual "[4,5,6] => 4" (4) (getFirst [4,5,6]))
test2 = TestCase (assertEqual "['one','two']" ("one") (getFirst ["one","two"]))
test3 = TestCase (assertEqual "[3]" (3) (getFirst [3]))
tests = TestList [TestLabel "test1" test1,TestLabel "test2" test2,TestLabel "test3" test3]

main = do runTestTT tests

{-Solution: 
getFirst x = head x
-}

{-Code: 
getFirst x = 
-}