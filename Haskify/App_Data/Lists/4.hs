--Category: %Lists
--Title: %Get an element from the list
{-Description: 
Make a function 'getThird' that returns third element from given list.
-}
{-Hint:
Use '!!' operator.
-}

import Prelude hiding (IO)
import Test.HUnit

test1 = TestCase (assertEqual "[4,5,6] => 6" (6) (getThird [4,5,6]))
test2 = TestCase (assertEqual "[8,1,5,7,3,2,3] => 5" (5) (getThird [8,1,5,7,3,2,3]))
test3 = TestCase (assertEqual "[3,3,3] => 3" (3) (getThird [3,3,3]))
tests = TestList [TestLabel "test1" test1,TestLabel "test2" test2,TestLabel "test3" test3]

main = do runTestTT tests

{-Solution: 
getThird x = x !! 2
-}

{-Code: 
getThird x =
-}