--Category: %Lists
--Title: %Get last
{-Description: 
Make a function 'getLast' that returns the last element from the list.
-}
{-Hint:
Use the 'last' function.
-}

import Prelude hiding (IO)
import Test.HUnit

test1 = TestCase (assertEqual "[4,5,6,7,8,3] => 4" (3) (getLast [4,5,6,7,8,3]))
test2 = TestCase (assertEqual "['one','two']" ("two") (getLast ["one","two"]))
test3 = TestCase (assertEqual "[3]" (3) (getLast [3]))
tests = TestList [TestLabel "test1" test1,TestLabel "test2" test2,TestLabel "test3" test3]

main = do runTestTT tests

{-Solution: 
getLast x = last x
-}

{-Code: 
getLast x = 
-}