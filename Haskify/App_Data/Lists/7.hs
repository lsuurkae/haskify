--Category: %Lists
--Title: %Reverse the list
{-Description: 
Make a function 'reverseList' that returns the last element from the list.
-}
{-Hint:
Use the 'reverse' function.
-}

import Prelude hiding (IO)
import Test.HUnit

test1 = TestCase (assertEqual "[4,5,6,7,8,3]" ([3,8,7,6,5,4]) (reverseList [4,5,6,7,8,3]))
test2 = TestCase (assertEqual "['one','two']" (["two","one"]) (reverseList ["one","two"]))
test3 = TestCase (assertEqual "vea kruusa siil" ("liis asuurk aev") (reverseList "vea kruusa siil"))
test4 = TestCase (assertEqual "aiassadassaia" ("aiassadassaia") (reverseList "aiassadassaia"))
tests = TestList [TestLabel "test1" test1,TestLabel "test2" test2,TestLabel "test3" test3,TestLabel "test4" test4]

main = do runTestTT tests

{-Solution: 
reverseList x = reverse x
-}

{-Code: 
reverseList x = 
-}