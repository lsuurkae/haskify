--Category: %Lists
--Title: %Get tail
{-Description: 
As we know list has a head and a tail. We know that we can 
get the head using 'head' function. What about the tail?
Make a function 'getTail' that returns the tail of the list.
-}
{-Hint:
Use 'tail' function or
as we know list x == (x:xs), 
where x is the head and xs the body of the list.
-}

import Prelude hiding (IO)
import Test.HUnit

test1 = TestCase (assertEqual "[4,5,6]" ([5,6]) (getTail [4,5,6]))
test2 = TestCase (assertEqual "['one','two']" (["two"]) (getTail ["one","two"]))
test3 = TestCase (assertEqual "[3]" ([]) (getTail [3]))
tests = TestList [TestLabel "test1" test1,TestLabel "test2" test2,TestLabel "test3" test3]

main = do runTestTT tests

{-Solution: 
getTail x = tail x
-}

{-Code: 
getTail x = 
-}