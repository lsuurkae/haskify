--Category: %Lists
--Title: %Add two lists
{-Description: 
Make a function 'addLists' that takes in two lists and unites them.
-}
{-Hint:
Lists can be made united with ++.
-}

import Prelude hiding (IO)
import Test.HUnit

test1 = TestCase (assertEqual "[1,2,3] and [4,5,6]" ([1,2,3,4,5,6]) (addLists [1,2,3] [4,5,6]))
test2 = TestCase (assertEqual "[1,1] and [8,8]" ([1,1,8,8]) (addLists [1,1] [8,8]))
test3 = TestCase (assertEqual "[] [3]" ([3]) (addLists [] [3]))
tests = TestList [TestLabel "test1" test1,TestLabel "test2" test2,TestLabel "test3" test3]

main = do runTestTT tests

{-Solution: 
addLists x y = x ++ y
-}

{-Code: 
addLists x y = 
-}
