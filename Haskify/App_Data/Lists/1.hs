--Category: %Lists
--Title: %Simple list
{-Description: 
Make a function 'simpleList' that returns the list of numbers 1,2,3
-}
{-Hint:
Lists can be made [1,2] or 1:2:[].
[1,2] is a syntactic sugar for 1:2:[]. 
-}

import Prelude hiding (IO)
import Test.HUnit

test1 = TestCase (assertEqual "[1,2,3]" ([1,2,3]) (simpleList))
tests = TestList [TestLabel "test1" test1]

main = do runTestTT tests

{-Solution: 
simpleList = [1,2,3]
-}

{-Code: 
simpleList = 
-}
