﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Haskify.Constants
{
    public class Resources
    {
        public static List<string> Categories = new List<string> {"Math","Lists","Syntax","Types","Recursion","High Order Functions","Monads", "Functors"}; 
    }
}