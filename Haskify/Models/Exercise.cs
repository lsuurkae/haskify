﻿using System.ComponentModel.DataAnnotations;

namespace Haskify.Models
{
    public class Exercise
    {
        public string FileName { get; set; }
        public string Title { get; set; }
        public string Category { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
        public string Hint { get; set; }
        public string Solution { get; set; }
    }
}