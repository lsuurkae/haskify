﻿using System.Collections.Generic;
using System.Web;

namespace Haskify.Models
{
    public class CategoryViewModel
    {
        public List<Category> Categories { get; set; }
        public string Introduction { get; set; }

        public string IsCategoryActive(string category)
        {
            var path = HttpContext.Current.Request.Url.AbsolutePath;
            string[] pathParameters = path.Split('/');
            if (pathParameters.Length < 2) return "";

            var activeCategory = pathParameters[1];
            if (string.IsNullOrEmpty(activeCategory)) return "";

            var isActive = activeCategory == category ? "activeMenu" : "";
            return isActive;
        }

        public string IsExerciseActive(string exercise, string category)
        {
            var path = HttpContext.Current.Request.Url.AbsolutePath;
            string[] pathParameters = path.Split('/');

            if (pathParameters.Length < 2)
                return "";

            var activeCategory = pathParameters[1];

            var activeExercise = "Introduction";

            if(pathParameters.Length >= 3)
                activeExercise = pathParameters[2];
            if (string.IsNullOrEmpty(activeExercise) && string.IsNullOrEmpty(activeCategory))
                return "";

            var isActive = (activeExercise == exercise && activeCategory == category) ? "activeMenu" : "";
            return isActive;
        }
    }
}