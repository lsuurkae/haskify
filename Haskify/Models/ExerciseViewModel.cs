﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Haskify.Models
{
    public class ExerciseViewModel
    {

        public string ActiveCategory { get; set; }
        public IEnumerable<string> CategoryExercises { get; set; } 
        public Exercise ActiveExercise { get; set; }
        public string Result { get; set; }
        public string Introduction { get; set; }
        public string NextCategory { get; set; }
        public string NextExercise { get; set; }


        public void GetNextExercise()
        {
            var currentName = Path.GetFileNameWithoutExtension(ActiveExercise.FileName);
            var exercises = CategoryExercises.ToList();
            var nextExIndex = exercises.IndexOf(currentName) + 1;
            if (nextExIndex <= (exercises.Count - 1))
            {
                NextExercise = exercises[nextExIndex];
                NextCategory = ActiveCategory;
            }
            else
            {
                NextExercise = "";
            }
        }

        public string IsActive(string exercise)
        {
            if (ActiveExercise == null) return "";

            var isActive = ActiveExercise.FileName == (exercise + ".hs") ? "active" : "";
            return isActive;
        }

        public string IsActiveCategory(string category)
        {
            if (string.IsNullOrEmpty(ActiveCategory)) return "";

            var isActive = ActiveCategory == category ? "active" : "";
            return isActive;
        }
    }
}