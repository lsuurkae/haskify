﻿using System.Collections.Generic;

namespace Haskify.Models
{
    public class Category
    {
        public string CategoryName { get; set; }
        public List<string> CategoryExercises { get; set; } 
    }
}   