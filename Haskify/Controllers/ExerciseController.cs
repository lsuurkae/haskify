﻿using System.Collections.Generic;
using System.Web.Mvc;
using Haskify.Models;
using Haskify.Services;

namespace Haskify.Controllers
{
    public class ExerciseController : Controller
    {
        private IExerciseService _exerciseService;
        private string _title = "Haskify";
        private List<Category> _categories;

        public ExerciseController(IExerciseService exerciseService)
        {
            _exerciseService = exerciseService;
            _categories = new List<Category>();

            GetCategoryExercises();
        }

        private void GetCategoryExercises()
        {
            var categories = _exerciseService.GetAllCategories();

            foreach (var cat in categories)
            {
                var categoryExercises = _exerciseService.GetAllExercisesFromCategory(cat);
                _categories.Add(new Category()
                {
                    CategoryName = cat,
                    CategoryExercises = categoryExercises
                });
            }
        }

        // GET: Tutorial
        public ActionResult Index()
        {
            var model = new ExerciseViewModel();
            
            ViewBag.Title = _title;
            return View(model);
        }

        [Route("{category}")]
        public ActionResult CategoryPage(string category)
        {
            var exercises = _exerciseService.GetAllExercisesFromCategory(category);
            var introduction = _exerciseService.GetIntro(category);

            var model = new ExerciseViewModel
            {
                CategoryExercises = exercises,
                ActiveCategory = category,
                Introduction = introduction
            };

            ViewBag.Title = _title;
            return View("CategoryPage", model);
        }

        [Route("{category}/{exerciseName}/")]
        public ActionResult ExercisePage(string category, string exerciseName)
        {
            var exercise = _exerciseService.GetExercise(category, exerciseName);
            var exercises = _exerciseService.GetAllExercisesFromCategory(category);

            var model = new ExerciseViewModel
            {
                CategoryExercises = exercises,
                ActiveCategory = category,
                ActiveExercise = exercise,
                NextCategory = _exerciseService.GetNextCategory(category)
            };
            model.GetNextExercise();
            ViewBag.Title = _title;
            return View("ExercisePage", model);
        }

        public ActionResult GetCategories()
        {
            var model = new CategoryViewModel()
            {
                Categories = _categories
            };

            return PartialView("_Categories", model);
        }

        [HttpPost]
        [Route("{category}/{exerciseName}")]
        public ActionResult PostSolution(string category, string exerciseName, string userSolution)
        {
            var exercise = _exerciseService.GetExercise(category, exerciseName);
            var result = _exerciseService.CheckSolution(exercise, userSolution);
            var exercises = _exerciseService.GetAllExercisesFromCategory(category);

            var model = new ExerciseViewModel()
            {
                Result = result,
                ActiveExercise = exercise,
                CategoryExercises = exercises,
                ActiveCategory = category
            };
            model.GetNextExercise();

            return View("ExercisePage", model);
        }
    }
}