﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using Haskify.Models;

namespace Haskify.Services
{
    public interface IExerciseService
    {
        List<string> GetAllCategories();
        List<string> GetAllExercisesFromCategory(string category);
        string GetIntro(string category);
        Exercise GetExercise(string category, string fileName);
        string CheckSolution(Exercise exercise, string userSolution);
        string GetNextCategory(string category);
    }

    public class ExerciseService : IExerciseService
    {
        private readonly string _exercisePath;

        private IHaskellService _haskellService;

        public ExerciseService(IHaskellService haskellService)
        {
            _haskellService = haskellService;
            _exercisePath = System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/");
        }

        public List<string> GetAllCategories()
        {
            var categories = Constants.Resources.Categories;

            return categories;
        }

        public List<string> GetAllExercisesFromCategory(string category)
        {
            var directory = new DirectoryInfo(_exercisePath + category);
            var files = directory.GetFiles("*.hs")
                .Select(f => Path.GetFileNameWithoutExtension(f.Name))
                .OrderBy(int.Parse)
                .ToList();

            return files;
        }

        public string GetIntro(string category)
        {
            var introduction = File.ReadAllLines(_exercisePath + category + @"\Intro.txt");
            return string.Join(" ", introduction);
        }

        public Exercise GetExercise(string category, string fileName)
        {
            var fileNameWithExtension = Path.GetFileNameWithoutExtension(fileName) + ".hs";

            string[] fileLines;
            try
            {
                fileLines = File.ReadAllLines(_exercisePath + category + @"\" + fileNameWithExtension);
            }
            catch (DirectoryNotFoundException)
            {
                return null;
            }

            var exercise = BuildExerciseModel(fileLines, category, fileNameWithExtension);

            return exercise;
        }

        public string CheckSolution(Exercise exercise, string userSolution)
        {
            if (exercise == null)
                return "error";

            exercise.Code = userSolution;

            var newFileName = _haskellService.CreateHaskellFile(exercise, _exercisePath);
            var compilationOutput = _haskellService.CompileHaskellFile(newFileName, _exercisePath);
            var executionOutput = _haskellService.ExecuteHaskellFile(newFileName, _exercisePath);

            var result = ChangeOutput(newFileName, executionOutput, compilationOutput);

            Thread.Sleep(500);
            DeleteUsedFiles(Path.GetFileNameWithoutExtension(newFileName));

            return result;
        }

        private string ChangeOutput(string fileName, string executionOutput, string compilationOutput)
        {
            var expectedCompilationError = ".exe' is not recognized as an internal or external command";
            var expectedTestFailureError = "### Failure in";

            if (executionOutput.Contains(expectedCompilationError))
                return RemoveFileNameFromOutput(fileName, compilationOutput);

            if (executionOutput.Contains(expectedTestFailureError))
                return RemoveFileNameFromOutput(fileName, executionOutput);

            if (executionOutput.Contains("Sorry!"))
                return executionOutput;

            if(executionOutput.Contains("Stack overflow!"))
                return executionOutput;

            return "Correct!";
        }

        private string RemoveFileNameFromOutput(string fileName, string output)
        {
            var regex = new Regex($"{fileName}(.*?)\r\n");
            var regex2 = new Regex($"{fileName}(.*?): ");
            var regexOutput = regex.Replace(output, string.Empty);
            regexOutput = regex2.Replace(regexOutput, string.Empty);
            return regexOutput;
        }

        private void DeleteUsedFiles(string fileNameWithoutExtension)
        {
            var fileList = Directory.GetFiles(_exercisePath, fileNameWithoutExtension + ".*");
            foreach (var file in fileList)
            {
                File.Delete(file);
            }
        }

        private Exercise BuildExerciseModel(string[] fileLines, string category, string fileName)
        {
            var description = GetExercisePropertiesFromFile(fileLines, "Description");
            var code = GetExercisePropertiesFromFile(fileLines, "Code");
            var hint = GetExercisePropertiesFromFile(fileLines, "Hint");
            var solution = GetExercisePropertiesFromFile(fileLines, "Solution");
            var title = "";

            foreach (var line in fileLines.Where(line => line.Contains("Title: %")))
            {
                title = line.Split('%')[1];
                break;
            }

            var exercise = new Exercise
            {
                Category = category,
                Description = description,
                Code = code,
                FileName = fileName,
                Title = title,
                Hint = hint,
                Solution = solution
            };

            return exercise;
        }

        private string GetExercisePropertiesFromFile(IEnumerable<string> fileLines, string searchString)
        {
            var property = "";
            var isProperty = false;
            foreach (var line in fileLines)
            {
                if (line.Contains("-}") && isProperty)
                    isProperty = false;
                if (isProperty)
                    property += line + "\n";
                if (line.Contains(searchString + ":"))
                    isProperty = true;
            }
            return property;
        }

        public string GetNextCategory(string category)
        {
            var categories = GetAllCategories();
            var categoryIndex = categories.IndexOf(category);
            var nextCategory = "Math";

            if (categoryIndex < categories.Count - 1)
                nextCategory = categories[categoryIndex + 1];
            
            return nextCategory;
        }
    }
}