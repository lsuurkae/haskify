using System;
using System.Diagnostics;
using System.IO;
using Haskify.Models;

namespace Haskify.Services
{
    public interface IHaskellService
    {
        string CompileHaskellFile(string fileName, string exercisePath);
        string CreateHaskellFile(Exercise exercise, string exercisePath);
        string ExecuteHaskellFile(string fileName, string exercisePath);
    }

    public class HaskellService : IHaskellService
    {
        public string CreateHaskellFile(Exercise exercise, string exercisePath)
        {
            var fileName = Guid.NewGuid() + ".hs";

            var destFileName = exercisePath + fileName;
            File.Copy(
                sourceFileName: exercisePath + exercise.Category + @"\" + exercise.FileName,
                destFileName: destFileName,
                overwrite: true);

            using (StreamWriter sw = File.AppendText(destFileName))
            {
                sw.WriteLine(Environment.NewLine);
                sw.WriteLine(exercise.Code);
            }

            return fileName;
        }

        public string CompileHaskellFile(string fileName, string exercisePath)
        {
            var process = new Process();

            var startInfo = new ProcessStartInfo
            {
                UseShellExecute = false,
                RedirectStandardError = true,
                WorkingDirectory = exercisePath,
                FileName = "CMD.exe",
                Arguments = @"/c ghc " + fileName
            };

            process.StartInfo = startInfo;
            process.Start();

            var compilationOutput = process.StandardError.ReadToEnd();
            process.WaitForExit();

            return compilationOutput;
        }

        public string ExecuteHaskellFile(string fileName, string exercisePath)
        {
            var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(fileName);
            var newFileName = fileNameWithoutExtension + ".exe";
            var process = new Process();

            var startInfo = new ProcessStartInfo
            {
                UseShellExecute = false,
                RedirectStandardError = true,
                WorkingDirectory = exercisePath,
                FileName = "CMD.exe",
                Arguments = @"/c " + newFileName,
                Verb = "runas"
            };

            process.StartInfo = startInfo;

            try
            {
                process.Start();

                if (!process.WaitForExit(3000))
                {
                    process.Kill();
                    KillExe(newFileName, exercisePath);
                    return "Stack overflow!";
                }

                var fileExecutionOutput = process.StandardError.ReadToEnd();

                return fileExecutionOutput;
            }
            catch
            {
                return "Sorry! Something went wrong. Try again!";
            }
        }

        private void KillExe(string fileName, string exercisePath)
        {
            var process = new Process();

            var startInfo = new ProcessStartInfo
            {
                UseShellExecute = false,
                RedirectStandardError = true,
                WorkingDirectory = exercisePath,
                FileName = @"cmd.exe",
                Arguments = @"/c Taskkill /IM " + fileName + " /F",
                Verb = "runas",
            };

            process.StartInfo = startInfo;
            process.Start();
            process.WaitForExit(4000);
        }
    }
}